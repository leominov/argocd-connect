package version

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetVersion(t *testing.T) {
	assert.Equal(t, "0.0.0", GetVersion())
	Version = "1.0.0"
	assert.Equal(t, Version, GetVersion())
}
