package version

var (
	// Version будет перезаписана goreleaser
	Version string
)

func GetVersion() string {
	if len(Version) == 0 {
		return "0.0.0"
	}
	return Version
}
