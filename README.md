# ArgoCD Connect

Connect and verify cluster configuration.

## Connect

Подключение происходит по следующим шагам:

- Создание Service Account `argocd-manager`;
- Создание Role Binding `argocd-manager-role-binding` для Service Account `argocd-manager` на получение прав `tinkoff-namespace-admins`;
- Получение Secret из Service Account `argocd-manager`;
- Генерация Secret для описания конфигурации подключаемого кластера.

## Verify

Проверка корректного описания кластера происходит по наличию необходимых полей в Secret, а так же наличию подключения к API-серверу Kubernetes с указанным токеном и сертификатом.
 