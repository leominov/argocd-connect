FROM golang:1.14-alpine3.11 as builder
WORKDIR /go/src/gitlab-rnd.tcsbank.ru/devops/argocd-connect
COPY . .
RUN go build ./cmd/argocd-connect

FROM alpine:3.11
COPY --from=builder /go/src/gitlab-rnd.tcsbank.ru/devops/argocd-connect/argocd-connect /usr/local/bin/argocd-connect
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
ENTRYPOINT ["argocd-connect"]
