package main

import (
	"os"

	"gitlab-rnd.tcsbank.ru/devops/argocd-connect/internal/cmd"
)

func main() {
	if err := cmd.NewRootCmd(os.Args[1:]).Execute(); err != nil {
		os.Exit(1)
	}
	os.Exit(0)
}
