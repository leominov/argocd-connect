package clusterconfig

import (
	"bytes"
	"crypto/tls"
	"crypto/x509"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/util/yaml"
)

type ClusterConfig struct {
	CAData     []byte
	Name       string
	Namespaces []string
	Server     string
	Token      string
	Insecure   bool
}

type ConnectConfig struct {
	BearerToken     string          `json:"bearerToken"`
	TLSClientConfig TLSClientConfig `json:"tlsClientConfig"`
}

type TLSClientConfig struct {
	Insecure bool   `json:"insecure"`
	CAData   string `json:"caData"`
}

func (c *ConnectConfig) JSON() string {
	b, err := json.Marshal(c)
	if err != nil {
		return ""
	}
	return string(b)
}

func (c *ClusterConfig) String() string {
	if len(c.Name) > 0 {
		return fmt.Sprintf("%s (%s)", c.Name, c.Server)
	}
	return c.Server
}

func (c *ClusterConfig) LoadFromSecret(secret *corev1.Secret) error {
	// Имя кластера
	if v, ok := secret.Data["name"]; ok {
		c.Name = string(v)
	} else {
		return errors.New("cluster name not defined")
	}

	// Список подключенных Namespace
	if v, ok := secret.Data["namespaces"]; ok {
		c.Namespaces = strings.Split(string(v), ",")
	}

	// Адрес сервера
	if v, ok := secret.Data["server"]; ok {
		c.Server = string(v)
	} else {
		return errors.New("cluster address not defined")
	}

	// Токен и CA
	if v, ok := secret.Data["config"]; ok {
		connectConfig := &ConnectConfig{}
		err := json.Unmarshal(v, connectConfig)
		if err != nil {
			return fmt.Errorf("failed to parse cluster configuration: %v", err)
		}

		c.Token = connectConfig.BearerToken
		c.Insecure = connectConfig.TLSClientConfig.Insecure

		data, err := base64.StdEncoding.DecodeString(connectConfig.TLSClientConfig.CAData)
		if err != nil {
			return err
		}
		c.CAData = data
	} else {
		return errors.New("cluster configuration not defined")
	}

	return nil
}

func LoadFromReader(reader io.Reader) ([]*ClusterConfig, error) {
	configs := []*ClusterConfig{}
	decoder := yaml.NewYAMLOrJSONDecoder(reader, 100)
	for {
		secret := &corev1.Secret{}
		err := decoder.Decode(secret)
		if err != nil {
			break
		}

		// Пропускаем, если имя ресурса отсутствует
		if len(secret.Name) == 0 {
			continue
		}

		config := &ClusterConfig{}
		err = config.LoadFromSecret(secret)
		if err != nil {
			return nil, err
		}
		configs = append(configs, config)
	}

	return configs, nil
}

func LoadFromFile(filename string) ([]*ClusterConfig, error) {
	b, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	return LoadFromReader(bytes.NewReader(b))
}

type VerifyAccessOptions struct {
	Insecure bool
	Timeout  time.Duration
}

func (c *ClusterConfig) transport(insecure bool) *http.Transport {
	caCertPool := x509.NewCertPool()
	caCertPool.AppendCertsFromPEM(c.CAData)

	return &http.Transport{
		TLSClientConfig: &tls.Config{
			RootCAs:            caCertPool,
			InsecureSkipVerify: insecure,
		},
	}
}

func (c *ClusterConfig) verifyAccess(opts *VerifyAccessOptions) error {
	req, err := http.NewRequest(http.MethodGet, fmt.Sprintf("%s/api", c.Server), nil)
	if err != nil {
		return err
	}
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", c.Token))

	transport := c.transport(opts.Insecure)

	client := &http.Client{
		Transport: transport,
		Timeout:   opts.Timeout,
	}

	resp, err := client.Do(req)
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("unexpected response: %s", resp.Status)
	}

	return nil
}

func (c *ClusterConfig) VerifyAccess(opts *VerifyAccessOptions) error {
	return c.verifyAccess(opts)
}
