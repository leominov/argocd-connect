package connect

import (
	"io/ioutil"
	"net/url"
	"os"
	"path"
	"testing"

	"github.com/stretchr/testify/assert"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes/fake"
)

func TestNewCmd(t *testing.T) {
	assert.NotNil(t, NewCmd())
}

func TestOptions_Run(t *testing.T) {
	dir, err := ioutil.TempDir("", "argocd")
	if assert.NoError(t, err) {
		defer os.RemoveAll(dir)
	}

	o := &Options{
		Namespace:      "foobar",
		secretName:     "foobar",
		outputFilename: path.Join(dir, "out.yaml"),
	}
	o.cli = fake.NewSimpleClientset()
	o.host = &url.URL{
		Scheme: "https",
		Host:   "127.0.0.1:8080",
	}
	err = o.Run()
	if assert.Error(t, err) {
		assert.Contains(t, err.Error(), "not found")
	}

	o.cli = fake.NewSimpleClientset(&v1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: "foobar",
		},
	})
	err = o.Run()
	if assert.Error(t, err) {
		assert.Contains(t, err.Error(), "not secrets")
	}

	o.cli = fake.NewSimpleClientset(&v1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: "foobar",
		},
	}, &v1.ServiceAccount{
		ObjectMeta: metav1.ObjectMeta{
			Name:      ArgoCDManagerServiceAccount,
			Namespace: "foobar",
		},
		Secrets: []v1.ObjectReference{
			{
				Name: "secret",
			},
		},
	})
	err = o.Run()
	if assert.Error(t, err) {
		assert.Contains(t, err.Error(), "not found")
	}

	o.cli = fake.NewSimpleClientset(&v1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: "foobar",
		},
	}, &v1.ServiceAccount{
		ObjectMeta: metav1.ObjectMeta{
			Name:      ArgoCDManagerServiceAccount,
			Namespace: "foobar",
		},
		Secrets: []v1.ObjectReference{
			{
				Name: "secret",
			},
		},
	}, &v1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name:      "secret",
			Namespace: "foobar",
		},
		Data: map[string][]byte{},
		Type: "kubernetes.io/service-account-token",
	})
	err = o.Run()
	if assert.Error(t, err) {
		assert.Contains(t, err.Error(), "ca.crt")
		assert.Contains(t, err.Error(), "secret not found")
	}

	o.cli = fake.NewSimpleClientset(&v1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: "foobar",
		},
	}, &v1.ServiceAccount{
		ObjectMeta: metav1.ObjectMeta{
			Name:      ArgoCDManagerServiceAccount,
			Namespace: "foobar",
		},
		Secrets: []v1.ObjectReference{
			{
				Name: "secret",
			},
		},
	}, &v1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name:      "secret",
			Namespace: "foobar",
		},
		Data: map[string][]byte{
			"ca.crt":    {},
			"token":     {},
			"namespace": {},
		},
		Type: "kubernetes.io/service-account-token",
	})
	err = o.Run()
	assert.NoError(t, err)

	gen, err := ioutil.ReadFile(o.outputFilename)
	assert.NoError(t, err)

	orig, err := ioutil.ReadFile("test_data/out.yaml")
	assert.NoError(t, err)

	assert.Equal(t, string(orig), string(gen))
}

func TestOptions_Process(t *testing.T) {
	o := NewOptions()
	o.Namespace = "foobar"
	o.host = &url.URL{
		Scheme: "https",
		Host:   "127.0.0.1:8080",
	}
	err := o.Process()
	assert.NoError(t, err)

	assert.Equal(t, "cluster-127.0.0.1-foobar.yaml", o.outputFilename)

	b, err := ioutil.ReadFile("test_data/out.yaml")
	assert.NoError(t, err)

	err = ioutil.WriteFile("cluster-127.0.0.1-foobar.yaml", b, os.ModePerm)
	if assert.NoError(t, err) {
		defer os.RemoveAll("cluster-127.0.0.1-foobar.yaml")
	}

	err = o.Process()
	if assert.Error(t, err) {
		assert.Contains(t, err.Error(), "already exists")
	}

	o.RewriteOutputFile = true
	err = o.Process()
	if assert.NoError(t, err) {
		assert.Equal(t, "foobar", o.secretName)
	}
}
