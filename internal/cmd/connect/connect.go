package connect

import (
	"bytes"
	"context"
	"encoding/base64"
	"errors"
	"fmt"
	"io/ioutil"
	"net/url"
	"strings"
	"time"

	"github.com/ghodss/yaml"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	corev1 "k8s.io/api/core/v1"
	rbacv1 "k8s.io/api/rbac/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	kyaml "k8s.io/apimachinery/pkg/util/yaml"
	"k8s.io/client-go/kubernetes"
	_ "k8s.io/client-go/plugin/pkg/client/auth/gcp"
	_ "k8s.io/client-go/plugin/pkg/client/auth/oidc"
	"k8s.io/client-go/tools/clientcmd"

	"gitlab-rnd.tcsbank.ru/devops/argocd-connect/internal/clusterconfig"
)

const (
	ArgoCDManagerRoleBinding    = "argocd-manager-role-binding"
	ArgoCDManagerServiceAccount = "argocd-manager"
)

var (
	pathOpts = clientcmd.NewDefaultPathOptions()
)

type Options struct {
	Namespace         string
	RewriteOutputFile bool
	RoleRef           rbacv1.RoleRef
	Stdout            bool

	cli            kubernetes.Interface
	host           *url.URL
	outputFilename string
	secretName     string
}

func NewOptions() *Options {
	return &Options{}
}

func NewCmd() *cobra.Command {
	o := NewOptions()

	cmd := &cobra.Command{
		Use:   "connect",
		Short: "Connect cluster to ArgoCD",
		RunE: func(cmd *cobra.Command, args []string) error {
			if err := o.Validate(); err != nil {
				return err
			}
			if err := o.Connect(); err != nil {
				return err
			}
			if err := o.Process(); err != nil {
				return err
			}
			if err := o.Run(); err != nil {
				return err
			}
			return o.Notice()
		},
	}

	flag := cmd.PersistentFlags()
	flag.BoolVar(&o.RewriteOutputFile, "rewrite", false, "Rewrite output configuration file")
	flag.BoolVar(&o.Stdout, "stdout", false, "Prints output configuration to stdout")
	flag.StringVar(&o.RoleRef.APIGroup, "role-ref-api-group", "rbac.authorization.k8s.io", "APIGroup for role reference")
	flag.StringVar(&o.RoleRef.Kind, "role-ref-kind", "ClusterRole", "Kind for role reference")
	flag.StringVar(&o.RoleRef.Name, "role-ref-name", "tinkoff-namespace-admins", "Name for role reference")
	flag.StringVarP(&o.Namespace, "namespace", "n", "default", "System namespace")

	return cmd
}

func (o *Options) Validate() error {
	if len(o.Namespace) == 0 {
		return errors.New("namespace not defined")
	}
	return nil
}

func (o *Options) Connect() error {
	var configAccess clientcmd.ConfigAccess = pathOpts

	config, err := configAccess.GetStartingConfig()
	if err != nil {
		return err
	}

	clientConfig := clientcmd.NewDefaultClientConfig(*config, nil)
	conf, err := clientConfig.ClientConfig()
	if err != nil {
		return err
	}

	o.host, err = url.Parse(conf.Host)
	if err != nil {
		return err
	}

	clientset, err := kubernetes.NewForConfig(conf)
	if err != nil {
		return err
	}

	o.cli = clientset

	return nil
}

func (o *Options) Process() error {
	o.outputFilename = fmt.Sprintf(
		"cluster-%s-%s.yaml",
		o.host.Hostname(),
		o.Namespace,
	)

	o.secretName = fmt.Sprintf(
		"cluster-%s-%s-%d",
		o.host.Hostname(),
		o.Namespace,
		time.Now().Unix(),
	)

	b, err := ioutil.ReadFile(o.outputFilename)
	if err == nil {
		if !o.RewriteOutputFile && !o.Stdout {
			return fmt.Errorf("file %q already exists", o.outputFilename)
		}
		secret := &corev1.Secret{}
		err := kyaml.NewYAMLOrJSONDecoder(bytes.NewReader(b), 100).Decode(secret)
		if err == nil {
			o.secretName = secret.Name
		}
	}

	return nil
}

func (o *Options) Run() error {
	logrus.Infof("Cluster %q – OK", o.host.String())

	namespaces := o.cli.CoreV1().Namespaces()

	// Проверяем, есть ли Namespace в кластере
	_, err := namespaces.Get(context.TODO(), o.Namespace, metav1.GetOptions{})
	if err != nil {
		return err
	}
	logrus.Infof("Namespace %q - OK", o.Namespace)

	// Проверяем, существует ли ServiceAccount
	accounts := o.cli.CoreV1().ServiceAccounts(o.Namespace)
	sa, err := accounts.Get(context.TODO(), ArgoCDManagerServiceAccount, metav1.GetOptions{})
	if err != nil {
		if !strings.Contains(err.Error(), "not found") {
			return err
		}

		sa, err = accounts.Create(context.TODO(), &corev1.ServiceAccount{
			TypeMeta: metav1.TypeMeta{
				APIVersion: "v1",
				Kind:       "ServiceAccount",
			},
			ObjectMeta: metav1.ObjectMeta{
				Name:      ArgoCDManagerServiceAccount,
				Namespace: o.Namespace,
			},
		}, metav1.CreateOptions{})
		if err != nil {
			return err
		}

		logrus.Infof("Service account %q created", ArgoCDManagerServiceAccount)

		// Запрашиваем созданный SA, к нему должен быть добавлен секрет
		sa, err = accounts.Get(context.TODO(), ArgoCDManagerServiceAccount, metav1.GetOptions{})
		if err != nil {
			return nil
		}
	} else {
		logrus.Infof("Service account %q – OK", ArgoCDManagerServiceAccount)
	}

	if sa == nil || len(sa.Secrets) == 0 {
		return fmt.Errorf("service account %q has not secrets", ArgoCDManagerServiceAccount)
	}

	secretRef := sa.Secrets[0]

	// Получаем информацию из секрета
	secrets := o.cli.CoreV1().Secrets(o.Namespace)
	secret, err := secrets.Get(context.TODO(), secretRef.Name, metav1.GetOptions{})
	if err != nil {
		return err
	}

	ca, ok := secret.Data["ca.crt"]
	if !ok {
		return fmt.Errorf("ca.crt in %q secret not found", secretRef.Name)
	}

	token, ok := secret.Data["token"]
	if !ok {
		return fmt.Errorf("token in %q secret not found", secretRef.Name)
	}

	// Проверяем наличие RoleBinding
	roleBindings := o.cli.RbacV1().RoleBindings(o.Namespace)
	_, err = roleBindings.Get(context.TODO(), ArgoCDManagerRoleBinding, metav1.GetOptions{})
	if err != nil {
		if !strings.Contains(err.Error(), "not found") {
			return err
		}

		_, err := roleBindings.Create(context.TODO(), &rbacv1.RoleBinding{
			TypeMeta: metav1.TypeMeta{
				APIVersion: "rbac.authorization.k8s.io/v1",
				Kind:       "RoleBinding",
			},
			ObjectMeta: metav1.ObjectMeta{
				Name:      ArgoCDManagerRoleBinding,
				Namespace: o.Namespace,
			},
			Subjects: []rbacv1.Subject{
				{
					Kind:      "ServiceAccount",
					Name:      ArgoCDManagerRoleBinding,
					Namespace: o.Namespace,
				},
			},
			RoleRef: o.RoleRef,
		}, metav1.CreateOptions{})
		if err != nil {
			return err
		}

		logrus.Infof("Role binding %q created", ArgoCDManagerRoleBinding)
	} else {
		logrus.Infof("Role binding %q – OK", ArgoCDManagerRoleBinding)
	}

	// Формируем ConnectConfig для конфигурации кластера
	caData := base64.StdEncoding.EncodeToString(ca)
	connectConfig := &clusterconfig.ConnectConfig{
		BearerToken: string(token),
		TLSClientConfig: clusterconfig.TLSClientConfig{
			Insecure: false,
			CAData:   caData,
		},
	}

	// Формируем конфигурацию для итогового секрета
	clusterName := fmt.Sprintf(
		"%s-%s",
		o.host.Hostname(),
		o.Namespace,
	)
	clusterSecret := corev1.Secret{
		TypeMeta: metav1.TypeMeta{
			APIVersion: "v1",
			Kind:       "Secret",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      o.secretName,
			Namespace: "argocd",
			Annotations: map[string]string{
				"managed-by": "argocd.argoproj.io",
			},
			Labels: map[string]string{
				"argocd.argoproj.io/secret-type": "cluster",
			},
		},
		Data: map[string][]byte{
			"config":     []byte(connectConfig.JSON()),
			"name":       []byte(clusterName),
			"namespaces": []byte(o.Namespace),
			"server":     []byte(o.host.String()),
		},
		Type: "Opaque",
	}

	b, err := yaml.Marshal(clusterSecret)
	if err != nil {
		return err
	}

	if o.Stdout {
		fmt.Println(string(b))
		return nil
	}

	separator := "---\n"
	separator += "# This is a generated file. Do not edit directly.\n"
	separator += "#\n"
	separator += "# Verify command:\n"
	separator += fmt.Sprintf("# $ argocd-connect verify --name %s\n", clusterName)
	separator += "# Connect command:\n"
	separator += fmt.Sprintf("# $ argocd-connect connect --namespace %s\n", o.Namespace)
	err = ioutil.WriteFile(o.outputFilename, append([]byte(separator), b...), 0644)
	if err != nil {
		return err
	}

	logrus.Infof("File %q saved – OK", o.outputFilename)

	return nil
}

func (o *Options) Notice() error {
	// Проверяем, указан ли результирующий файл в kustomization.yaml, если не указан,
	// либо файл не найден - предупреждаем о необходимости добавления
	b, err := ioutil.ReadFile("kustomization.yaml")
	if err == nil {
		if bytes.Contains(b, []byte(o.outputFilename)) {
			return nil
		}
	}

	logrus.Warnf("Don't forget to include %q file to resources list in %q file!\n",
		o.outputFilename, "kustomization.yaml")

	return nil
}
