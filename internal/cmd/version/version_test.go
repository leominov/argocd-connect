package version

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewCmd(t *testing.T) {
	assert.NotNil(t, NewCmd())
}

func TestRun(t *testing.T) {
	assert.NoError(t, Run())
}
