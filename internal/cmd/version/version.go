package version

import (
	"fmt"

	"github.com/spf13/cobra"

	"gitlab-rnd.tcsbank.ru/devops/argocd-connect/version"
)

func NewCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "version",
		Short: "Print version information",
		RunE: func(cmd *cobra.Command, args []string) error {
			return Run()
		},
	}

	return cmd
}

func Run() error {
	fmt.Println(version.GetVersion())
	return nil
}
