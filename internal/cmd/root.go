package cmd

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"

	"gitlab-rnd.tcsbank.ru/devops/argocd-connect/internal/cmd/connect"
	"gitlab-rnd.tcsbank.ru/devops/argocd-connect/internal/cmd/verify"
	"gitlab-rnd.tcsbank.ru/devops/argocd-connect/internal/cmd/version"
)

func init() {
	logrus.SetFormatter(&logrus.TextFormatter{
		ForceColors: true,
	})
}

func NewRootCmd(_ []string) *cobra.Command {
	rootCmd := &cobra.Command{
		Use:          "argocd-connect",
		SilenceUsage: true,
	}

	rootCmd.AddCommand(
		verify.NewCmd(),
		connect.NewCmd(),
		version.NewCmd(),
	)

	return rootCmd
}
