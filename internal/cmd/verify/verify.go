package verify

import (
	"errors"
	"path/filepath"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"

	"gitlab-rnd.tcsbank.ru/devops/argocd-connect/internal/clusterconfig"
)

type Options struct {
	ConfigFiles  []string
	ClusterNames []string
	verifyConfig *clusterconfig.VerifyAccessOptions
}

func NewOptions() *Options {
	return &Options{
		verifyConfig: &clusterconfig.VerifyAccessOptions{},
	}
}

func NewCmd() *cobra.Command {
	o := NewOptions()

	cmd := &cobra.Command{
		Use:   "verify",
		Short: "Verify cluster configuration",
		RunE: func(cmd *cobra.Command, args []string) error {
			if err := o.Validate(); err != nil {
				return err
			}
			return o.Run()
		},
	}

	flag := cmd.PersistentFlags()
	flag.StringArrayVarP(&o.ConfigFiles, "config", "c", []string{}, "Path to configuration file with clusters")
	flag.StringArrayVarP(&o.ClusterNames, "name", "n", []string{}, "Filter by cluster name")
	flag.BoolVarP(&o.verifyConfig.Insecure, "insecure", "k", false, "Allow insecure server connections when using SSL")
	flag.DurationVarP(&o.verifyConfig.Timeout, "max-time", "m", 5*time.Second, "Maximum time allowed for the transfer")

	return cmd
}

func findClusterConfigFiles() ([]string, error) {
	matches, _ := filepath.Glob("cluster-*")
	if len(matches) == 0 {
		return nil, errors.New("config not defined")
	}
	return matches, nil
}

func (o *Options) Validate() error {
	if len(o.ConfigFiles) == 0 {
		configFiles, err := findClusterConfigFiles()
		if err != nil {
			return err
		}
		logrus.Infof("Found cluster configuration files: %s",
			strings.Join(configFiles, " "))
		o.ConfigFiles = configFiles
	}
	return nil
}

func (o *Options) inClusterNames(name string) bool {
	if len(o.ClusterNames) == 0 {
		return true
	}
	for _, clusterName := range o.ClusterNames {
		if clusterName == name {
			return true
		}
	}
	return false
}

func (o *Options) Run() error {
	var failed bool

	for _, configFile := range o.ConfigFiles {
		clusters, err := clusterconfig.LoadFromFile(configFile)
		if err != nil {
			return err
		}
		for _, cluster := range clusters {
			if !o.inClusterNames(cluster.Name) {
				continue
			}
			err := cluster.VerifyAccess(o.verifyConfig)
			if err != nil {
				failed = true
				logrus.Errorf("%s – %v", cluster.String(), err)
				continue
			}
			logrus.Infof("%s – %s", cluster.String(), "OK")
		}
	}

	if failed {
		return errors.New("failed")
	}

	return nil
}
