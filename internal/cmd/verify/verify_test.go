package verify

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewCmd(t *testing.T) {
	assert.NotNil(t, NewCmd())
}

func TestOptions_Validate(t *testing.T) {
	o := NewOptions()
	assert.Error(t, o.Validate())
	o.ConfigFiles = []string{
		"foobar",
	}
	assert.NoError(t, o.Validate())
}

func TestOptions_Run(t *testing.T) {
	o := NewOptions()
	o.ConfigFiles = []string{
		"test_data/cluster.yaml",
	}
	assert.NoError(t, o.Validate())
	err := o.Run()
	if assert.Error(t, err) {
		assert.Contains(t, err.Error(), "failed")
	}
}
