module gitlab-rnd.tcsbank.ru/devops/argocd-connect

go 1.14

require (
	github.com/ghodss/yaml v1.0.0
	github.com/sirupsen/logrus v1.7.0
	github.com/spf13/cobra v1.1.1
	github.com/stretchr/testify v1.4.0
	k8s.io/api v0.19.3
	k8s.io/apimachinery v0.19.3
	k8s.io/client-go v0.19.2
)
